
-- Show All Databases
show databases;

-- Create New Database
create database music_db;

-- Delete Database
drop database music_db;

-- Select Database
use music_db;

-- Create Table 
create table user_accounts (
--  label DataType Required/Default UniqueIdentifier
	id int not null auto_increment, 
	username varchar(50) not null,
	password varchar(50) not null,
	full_name varchar(50) not null,
	contact_number int not null,
	email varchar(50), 
	address varchar(50), 
	primary key(id)
);

create table artist (
	id int not null auto_increment, 
	name varchar(50) not null,
	primary key (id)
);

create table albums (
	id int not null auto_increment, 
	album_title varchar(50) not null,
	date_released date not null,
	artist_id int not null,
	primary key (id),
	-- Costraint / Rules
	constraint fk_albums_artist_id
		foreign key (artist_id) references artist(id) 
		on update cascade
		on delete restrict
);

create table songs (
	id int not null auto_increment, 
	song_name varchar(50) not null,
	length time not null,
	genre varchar(50) not null,
	album_id int not null
	primary key (id),
	constraint fk_songs_album_id 
		foreign key(album_id) references albums(id)
		on update cascade
		on delete restrict
);

create table playlists(
	id int not null auto_increment,
	user_id int not null,
	dateTime_created datetime not null,
	 primary key(id),
	 constraint fk_playlists_users_id
	 	foreign key (user_id) references user_accounts(id)
	 	on update cascade
	 	on delete restrict
);

create table playlists_songs (
	id int not null auto_increment,
	playlist_id	 int not null,
	song_id int not null,
	primary key(id),
	constraint fk_playlists_songs_playlist_id 
		foreign key(playlist_id) references playlists(id)
		on update cascade
		on delete restrict,
	constraint fk_playlists_songs_song_id 
		foreign key(song_id) references songs(id)
		on update cascade
		on delete restrict
);