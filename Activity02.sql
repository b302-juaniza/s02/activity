create database blog_db;

use blog_db;

create table users(
	id int not null auto_increment,
	email varchar(100) not null,
	password varchar(100) not null,
	dateTime_created datetime not null,
	primary key(id)
);

create table posts (
	id int not null auto_increment, 
	author_id int not null,
	title varchar(500) not null,
	content varchar(5000) not null,
	dateTime_posted datetime not null,
	primary key (id),
	constraint fk_posts_author_id
		foreign key (author_id) references users(id) 
		on update cascade
		on delete restrict
);

create table post_comments (
	id int not null auto_increment, 
	post_id int not null,
	user_id int not null,
	content varchar(5000) not null,
	dateTime_commented datetime not null,
	primary key (id),
	constraint fk_posts_comments_post_id
		foreign key (post_id) references posts(id) 
		on update cascade
		on delete restrict,
	constraint fk_posts_comments_user_id
	foreign key (user_id) references users(id) 
	on update cascade
	on delete restrict
);

create table post_likes (
	id int not null auto_increment, 
	post_id int not null,
	user_id int not null,
	dateTime_liked datetime not null,
	primary key (id),
	constraint fk_posts_likes_post_id
		foreign key (post_id) references posts(id) 
		on update cascade
		on delete restrict,
	constraint fk_posts_likes_user_id
	foreign key (user_id) references users(id) 
	on update cascade
	on delete restrict
);

